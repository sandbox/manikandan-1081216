/* $Id: README.txt,v 1.7 2011/02/01 11:47:57 mani Exp $ */

Expression module allow a site administrator to manage expressions on the site
and allow site user to express their views on site content


Installation
------------

Copy express_it.module to your module directory and then enable on the admin
modules page.  Add expressions & manage node types at admin/settings/express,
then view the expression on each node (By default its assigned to node type).

Author
------
Manikandan
mani.stelli@gmail.com

I've used the free script to process image. (i.e, ImageProcessing - http://www.white-hat-web-design.co.uk/articles/php-image-resizing.php)
