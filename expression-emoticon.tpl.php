<?php
// $Id: expression-emoticon.tpl,v 1.297.2.4 2011/02/01 16:20:38 mani Exp $

/**
 * @file
 * Expression image template file for each node
 * 
 * @params
 * expressions - All expressions
 */

foreach($expressions as $expression): 

$file_name = $image_path."/".$expression["file"];
if(!file_exists($file_name))
	$file_name = $alt_image_path."/".$expression["file"];
?>
<div>
<span><a href="#" id="<?php print $expression["eid"]; ?>" v_nid="<?php print $nid; ?>" class="exp" token="<?php print $expression["token"]; ?>"><img src="<?php print $file_name; ?>" title="<?php print $expression["name"]; ?>" alt="<?php print $expression["name"]; ?>" /></a></span>
<span id="excount_<?php print $expression["eid"] . "_" . $nid; ?>"><?php print $expression["excount"]; ?></span>
</div>
<?php endforeach; ?>

